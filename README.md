# Mind Geek

***** Exercice donné par Ariette sama pour apprendre l'intégration !!!! ******

## Procédure pour créer une branche git

1.Se déplacer dans le dossier racine du projet. 
Vérifier que l'on se trouve bien dans la branche master avec un:
```git status```
si non :
``git checkout master`` 

2. Se mettre à jour avec master :
``git pull origin master``

3. Créer la branche
``git checkout -b nom-de-la-branche``

4. Coder



## Pocédure de sauvegarde de fonctionnalités

1. Se déplacer dans le dossier racine du projet. 

2. Ajouter mes modifs locales dans le "sas":
. ajouter juste un fichier : ``git add nomdufichier``
. ajouter tout : ``git add .``
. mettre a jour les fichiers deja dans le "sas" : ``git add -p``

3. sauvegarder les modifications présentes dans le "sas" pour les pusher:
``git commit -m "message explicatif du commit"``

4. Pusher les modifications vers gitlab:
`` git push origin nomdelabranche``

5. Faire une merge request (mr ou pr)
